/**
 * 统一导入工具类模块
 */
import getFormatTime from './module/TDateFormat.js';
import validateNumber from './module/TNumberType.js';
import chineseCheck from './module/TChineseCheck.js';
import validatePassWord from './module/TValidatePassWord.js';
import deepCloneObj from './module/TDeepCloneObj.js';
import validateEmail from './module/TValidateEmail.js';
import getTwoDecimal from './module/TKeepTwoDecimal.js';
import validateFormatPhone from './module/TValidatePhone.js';
import getHistoryTime from './module/THistoryTime.js';
import validateIdent from './module/TValidateIdent.js';
import multDeduplication from './module/TDeduplication.js';
import calcDateDifference from './module/TCalcDateDifference.js';
import groupByKey from './module/TGroupByKey.js';

/**
 * 单个方法暴露
 */
export {
    getFormatTime,
    validateNumber,
    chineseCheck,
    validatePassWord,
    deepCloneObj,
    validateEmail,
    getTwoDecimal,
    validateFormatPhone,
    getHistoryTime,
    validateIdent,
    multDeduplication,
    calcDateDifference,
    groupByKey
}

/**
 * 统一导出给外部调用
 */
export default {
    getFormatTime,
    validateNumber,
    chineseCheck,
    validatePassWord,
    deepCloneObj,
    validateEmail,
    getTwoDecimal,
    validateFormatPhone,
    getHistoryTime,
    validateIdent,
    multDeduplication,
    calcDateDifference,
    groupByKey
}