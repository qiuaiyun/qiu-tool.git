import { customLog } from "../utils/index.js";

// 规范时间格式变量
const YEAR_YYYY = 'YYYY';
const MONTH_YYYY_MM = 'YYYY-MM';
const DATE_YYYY_MM_DD = 'YYYY-MM-DD';
const DATETIME_YYYY_MM_DD_HH = 'YYYY-MM-DD HH';
const DATETIME_YYYY_MM_DD_HH_MM = 'YYYY-MM-DD HH:MM';
const DATETIME_YYYY_MM_DD_HH_MM_SS = 'YYYY-MM-DD HH:MM:SS';
const TIME_HH_MM_SS = 'HH:MM:SS';
const SEPARATE_TIME = ':';
const FORMAT_SET = [
    YEAR_YYYY, 
    MONTH_YYYY_MM, 
    DATE_YYYY_MM_DD, 
    DATETIME_YYYY_MM_DD_HH, 
    DATETIME_YYYY_MM_DD_HH_MM, 
    DATETIME_YYYY_MM_DD_HH_MM_SS, 
    TIME_HH_MM_SS
]

/**
 * @description 将标准时间转成需要格式返回，默认年月日时分秒格式，
 *              如果不传入时间或者格式，默认获取当前时间 YYYY-MM-DD HH:MM:SS 返回
 * @param {*} format 时间格式，默认：YYYY-MM-DD HH:MM:SS
 * @param {*} time 当前时间，js中一般由new Date()获得，格式示例：Sun Nov 28 2021 19:46:23 GMT+0800 (中国标准时间)
 * @returns string 年-月-日 xx:xx:xx 默认
 */
const getFormatTime = (time, format = DATETIME_YYYY_MM_DD_HH_MM_SS) => {
    if (time && typeof time !== 'string') {
        customLog(`传入时间字符串格式【${time}】错误，正确示例：2023、2023-02、2023-02-11、2023-02-11 10、2023-02-11 10:10、2023-02-11 10:10:10`)
        return;
    }
    if (format && typeof format !== 'string') {
        customLog(`传入时间格式错误【${format}】，正确示例：YYYY、YYYY-MM、YYYY-MM-DD、YYYY-MM-DD HH、YYYY-MM-DD HH:MM、YYYY-MM-DD HH:MM:SS、HH:MM:SS`)
        return;
    }

    const _FORMAT = format.toUpperCase();

    // 判断当前时间格式是否支持
    if (!FORMAT_SET.map(_EL => _EL.toUpperCase()).some(_e => _e === _FORMAT)) {
        customLog(`当前时间格式【${format}】不支持,正确示例：YYYY、YYYY-MM、YYYY-MM-DD、YYYY-MM-DD HH、YYYY-MM-DD HH:MM、YYYY-MM-DD HH:MM:SS、HH:MM:SS`);
        return
    }
    
    // 是否传入时间 new Date()
    const date = time ? new Date(time) : new Date();
    
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const hour = date.getHours();
    const minute = date.getMinutes();
    const second = date.getSeconds();

    // 存在分隔符
    let date_format = '';
    if (_FORMAT === YEAR_YYYY) {
        date_format = `${year}`;
    }
    if (_FORMAT === MONTH_YYYY_MM) {
        date_format = `${year}-${formatTen(month)}`
    }
    if (_FORMAT === DATE_YYYY_MM_DD) {
        date_format = `${year}-${formatTen(month)}-${formatTen(day)}`
    }
    if (_FORMAT === DATETIME_YYYY_MM_DD_HH) {
        date_format = `${year}-${formatTen(month)}-${formatTen(day)} ${formatTen(hour)}`
    }
    if (_FORMAT === DATETIME_YYYY_MM_DD_HH_MM) {
        date_format = `${year}-${formatTen(month)}-${formatTen(day)} ${formatTen(hour)}${SEPARATE_TIME}${formatTen(minute)}`
    }
    if (_FORMAT === DATETIME_YYYY_MM_DD_HH_MM_SS) {
        date_format = `${year}-${formatTen(month)}-${formatTen(day)} ${formatTen(hour)}${SEPARATE_TIME}${formatTen(minute)}${SEPARATE_TIME}${formatTen(second)}`
    }
    if (_FORMAT === TIME_HH_MM_SS) {
        date_format = `${formatTen(hour)}${SEPARATE_TIME}${formatTen(minute)}${SEPARATE_TIME}${formatTen(second)}`
    }
    if (!_FORMAT) {
        date_format = `${year}-${formatTen(month)}-${formatTen(day)} ${formatTen(hour)}${SEPARATE_TIME}${formatTen(minute)}${SEPARATE_TIME}${formatTen(second)}`
    }
    return date_format;
};

/**
 * 补充个位单数缺失数字
 */
const formatTen = num => num > 9 ? `${num}` : `0${num}`;
export {
    getFormatTime
}
export default getFormatTime;