import { customLog } from "../utils/index.js";

/**
 * 校验手机号格式是否正确
 * @param {*} phone 手机号 
 * @returns Boolean true正确 false错误
 */
const validateFormatPhone = (phone = '') => {
    if (!phone) {
        customLog('参数phone不能为空')
        return
    }
    const reg = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;
    return reg.test(phone);
}

export default validateFormatPhone;