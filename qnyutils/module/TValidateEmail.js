import { customLog } from "../utils/index.js";

/**
 * 验证邮箱
 * @param {*} value 邮箱值
 * @returns 
 */
 const validateEmail = value => {
    if (!value) {
        customLog('参数Email不能为空')
        return
    }
    
    let reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
    return !reg.test(value)? false:true;
}

export default validateEmail;