import { customLog, isCheckDateAndTime } from "../utils/index.js";

/**
 * 历史时间格式化，例如，几秒前，几天前，几分钟前
 * @param {*} value 时间字符串： 例如：2021-11-29 16:05:10 
 * @returns 
 */
const getHistoryTime = value => {
	// 判断是否为空
	if (!value) {
		customLog(`参数【${value}】不能为空，或者undefined或者null`)
		return
	}

	// 判断时间格式是否规范
	if (!isCheckDateAndTime(value)) {
		customLog(`参数【${value}】格式错误，参照示例：2023-11-29 16:05:10`)
		return
	}

	// 获取当前时间
	let dateEnd = new Date()
	// 时间格式转化为/，使用new Date()
	let dateBegin = new Date(value.replace(/-/g, '/'))
	// 时间差的毫秒数
	let dateDiff = dateEnd.getTime() - dateBegin.getTime()
	// 计算出相差天数
	let dayDiff = Math.floor(dateDiff / (24*3600*1000))
	if(dayDiff > 0){
		return `${dayDiff}天前`
	}
	
	// 计算当前时间和发布时间相差毫秒数
	let leave1 = dateDiff % (24*3600*1000)
	// 计算当前时间和发布时间相差小时数
	let hours = Math.floor(leave1/(3600*1000))
	if(hours > 0){
		return `${hours}小时前`
	}
	
	// 计算相差分钟数
	// 计算当前时间和发布时间小时后的相差毫秒数
	let leave2 = leave1 % (3600*1000)
	// 计算相差分钟数
	let minutes = Math.floor(leave2 / (60*1000))
	if(minutes > 0){
		return `${minutes}分钟前`
	}
	
	//计算相差秒数
	//计算分钟数后剩余的毫秒数
	let leave3 = leave2 % (60 * 1000) 
	let seconds = Math.round(leave3 / 1000)
	return `${seconds}秒前`
}

export {
	getHistoryTime
}

export default getHistoryTime;