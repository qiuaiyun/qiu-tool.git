/**
 *  @description 这仅仅是一个简单版本的深度拷贝，有很多边缘案例的错误
 *  @param { Object } source 克隆对象
 *  @returns { Object }
 */
 const deepCloneValue = source => {

    let sourceCopy = source instanceof Array ? [] : {}

    if (!source) sourceCopy = source

    for (let keys in source) {
        sourceCopy[keys] = source[keys] && typeof source[keys] === 'object' ? deepClone(source[keys]) : source[keys]
    }

    return sourceCopy
}

export default deepCloneValue;