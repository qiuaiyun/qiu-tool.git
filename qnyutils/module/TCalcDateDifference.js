import { 
    customLog, 
    isCheckDateAndTime 
} from "../utils/index.js";

/**
 * 计算时间区间差值，返回相差天数、小时和分钟
 * @param {*} startTime 开始时间
 * @param {*} endTime 结束时间
 * @returns 
 * {
 *    day: days,
 *    hours: hour,
 *    timeStr: time,
 *    minutes
 *  }
 */
const calcDateDifference = (startTime, endTime) => {
    // 判断时间是否为空
    if (!startTime || !endTime) {
        customLog(`startTime 和 endTime 不能为空`)
        return
    }
    // 判断日期格式
    if (!isCheckDateAndTime(startTime) || !isCheckDateAndTime(endTime)) {
        !isCheckDateAndTime(startTime) && customLog(`传入开始时间【${startTime}】格式错误`)
        !isCheckDateAndTime(endTime) && customLog(`传入结束时间【${endTime}】格式错误`)
        return
    }
    
    const stime = new Date(startTime).getTime();
    const etime = new Date(endTime).getTime();
    // 两个时间戳相差的毫秒数
    const usedTime = etime - stime;
    // 相差天数
    const days = Math.floor(usedTime / (24 * 3600 * 1000));
    //计算出小时数
    const leave1 = usedTime % (24 * 3600 * 1000);
    //计算天数后剩余的毫秒数
    // const hours = Math.floor(leave1 / (3600 * 1000));
    // 保留全部小数，返回时四舍五入截取，保证数据的精确性
    const hours = leave1 / (3600 * 1000);
    //计算相差分钟数
    const leave2 = leave1 % (3600 * 1000);
    //计算小时数后剩余的毫秒数
    const minutes = Math.floor(leave2 / (60 * 1000));
    const hour = +hours.toFixed(2);
    const time = `${days}天${hour}时${minutes}分钟`;
    return {
        day: days,
        hours: hour,
        timeStr: time,
        minutes
    };
}

export {
    calcDateDifference
};

export default calcDateDifference;