/**
 * 通用校验方法
 */
/**
 * 校验对象，数组，Boolean, 数字
 * @param {*} value 校验值
 */
export const validateType = (value = null)=>{
    let arr = ['boolean', 'object', 'number'], flag = true
    if(arr.includes(typeof value)){
        flag = false
    }
    if (value instanceof Array) {
        flag =  false
    }
    return flag
}

