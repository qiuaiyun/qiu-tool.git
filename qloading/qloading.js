"use strict"; // 启用严格模式
;(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.QLoading = factory();
    }
})(this, function(_params) {
    let QLoading = {}, iconObj = {
        circle: 'fa fa-spinner fa-spin fa-5x',
        spinner: 'fa fa-spinner fa-pulse fa-5x',
        notch: 'fa fa-circle-o-notch fa-spin fa-5x',
        refresh: 'fa fa-refresh fa-spin fa-5x',
        setting: 'fa fa-cog fa-spin fa-5x'
    }
    QLoading.version = "0.1.0";

    let Setting = QLoading.setting = {
        title: 'loading...',
        bgOpacity: '0.7',
        bgColor: '#4E8397',
        fontColor: "#fff",
        defineIcon: "spinner",
        zIndex: '9999',
        parent: "body",
        duration: 3000,
        templale: `<div class="qloading-item">
                        <i class="fa fa-spinner fa-pulse fa-5x qset-animation-size"></i>
                        <p class="qloading-title">loading...</p>
                    </div>
                    `
    }

    // _is_Render
    Object.defineProperty(Setting, '_is_Render',{
        value: true,
        enumerable: true,
        writable: true
    })

    /**
     * Updates configuration
     *  QLoading.configure({
     *     minimum: 0.1
     *  });
     */
    QLoading.configure = function(options) {
        let key, value;
        for(key in options){
            value = options[key]
            if(value !==undefined && options.hasOwnProperty(key)) Setting[key] = value;
        }
        return this
    }

    /**
     * Open pop-up window
     * QLoading.start()
     */
    QLoading.execute = function(params) {
        if(Setting._is_Render&&![8,9].includes(isSupportedAnimation())){
            QLoading.render()
            setTimeout(() => {
                QLoading.destroy()
            }, Setting.duration);
        }else {
            Setting._is_Render = false;
            throw new Error('The browser does not support CSS3 animation temporarily, and an error is displayed.')
        }
        return this
    }
    /**
     * Close pop-up window
     * QLoading.start()
     */
    QLoading.destroy = function(params) {
        let _q_loading_Element = document.getElementById('qloading-container'),
            _q_children_Element = document.querySelector('.qloading-item'),
            _q_qloading_custom_parent = document.querySelector('.qloading-custom-parent');
        if(_q_children_Element)_q_loading_Element.removeChild(_q_children_Element);
        if(_q_loading_Element)_q_loading_Element.remove();
        if(_q_qloading_custom_parent)_q_qloading_custom_parent.style.pointerEvents = "auto"
        console.log('destroy--load');
        
        // complate
        Setting._is_Render = true
        return this
    }

    /**
     * rendering 
     */
    QLoading.render = function(render) {
        if(QLoading.isRendered()) {
            Setting._is_Render = false
            return document.getElementById('qloading-container')
        }
        let loading = document.createElement('div');
            loading.id = "qloading-container";
            loading.setAttribute('class', 'test-bg')
            loading.innerHTML = Setting.templale

        // let parent = document.querySelector(Setting.parent)
        let parent = null
        // Setting.parent is null or empty
        if (!Setting.parent) {
            parent = document.getElementsByTagName('body')[0]
        }else {
            parent = document.querySelector(Setting.parent)
        }
        
        // render html
        if(parent&&parent !== document.body){
            addClass(parent, 'qloading-custom-parent');
            if(document.querySelector(Setting.parent))document.querySelector(Setting.parent).style.pointerEvents = "none"
        }
        if(!parent){
            throw new Error('Label with '+Setting.parent+' is not found，Please define it and try again.')
            return false
        }
        parent.appendChild(loading)
        setTimeout(()=>{
            let childrenItem = document.querySelector('.qloading-item'),
                hTitle = document.querySelector('.qloading-title'),
                parent_bg = document.getElementById('qloading-container');
                
                childrenItem.setAttribute('class', 'qloading-item actived')
                childrenItem.style.color = Setting.fontColor;
                replaceClassName(childrenItem.children[0], Setting.defineIcon)
                // removeClass(document.getElementById('qloading-container'), 'test-bg')
                hTitle.innerText = Setting.title;
                // set parent of value
                parent_bg.style.opacity = Setting.bgOpacity;
                parent_bg.style.backgroundColor = Setting.bgColor;
                parent_bg.style.zIndex = Setting.zIndex

        }, 10)
        return loading
    }

    /**
     * div is Rendered
     */
    QLoading.isRendered = function(params) {
        return !!document.getElementById('qloading-container')
    }

    // Determine whether there is an animation name
    function isAnimationName(name=null) {
        let flag = 0
        Object.keys(iconObj).forEach(key =>{
            if (name&&name === key) {
                flag++
            }
        })
        return flag;
    }
    // Replace class name
    function replaceClassName(element, newStr) {
        if(isAnimationName(newStr)){
            element.className = iconObj[newStr]+' qset-animation-size'
        }else {
            throw new Error('Animation name:'+newStr+'non-existent, Please reselect the animation name.')
        }
    }
    // Determine whether CSS3 animation is supported
    function isSupportedAnimation(params) {
        let userAgent = navigator.userAgent; //userAgent
        let isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //IE<11
        let isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //IE Edge 
        let isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
        if(isIE) {
            var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
            reIE.test(userAgent);
            var fIEVersion = parseFloat(RegExp["$1"]);
            if(fIEVersion == 7) {
                return 7;
            } else if(fIEVersion == 8) {
                return 8;
            } else if(fIEVersion == 9) {
                return 9;
            } else if(fIEVersion == 10) {
                return 10;
            } else {
                return 6;//IE<=7
            }   
        } else if(isEdge) {
            return 'edge';//edge
        } else if(isIE11) {
            return 11; //IE11  
        }else{
            return -1;//it`s not ie
        }
    }
    // define class name
    function addClass(element, name) {
        let oldList = classList(element),
            newList = oldList + name;

        if (hasClass(oldList, name)) return; 

        // Trim the opening space.
        element.className = newList.substring(1);
    }

    // Find class name location
    function hasClass(element, name) {
        let list = typeof element == 'string' ? element : classList(element);
        return list.indexOf(' ' + name + ' ') >= 0;
    }

    // return class list 
    function classList(element) {
        return (' ' + (element.className || '') + ' ').replace(/\s+/gi, ' ');
    }

    // Removes a class from an element
    function removeClass(element, name) {
        let oldClassStr = classList(element), newClssStr=null;
        // If it does not exist, it will not be deleted
        if(!hasClass(element, name)) return
        newClssStr = oldClassStr.replace(' '+name+' ',' ')
        element.className = oldClassStr.substring(1, oldClassStr.length-1);          
    }

    return QLoading;
})