import { DateFormat } from "./DateType";
declare class DateKit {
    date: Date;
    year: number;
    month: number;
    day: number;
    hour: number;
    minute: number;
    second: number;
    isTrue: boolean;
    private constructor();
    /**
     *  Set date by developer
     * @param _date YYYY-MM-DD HH:MM:SS or 2023-09-01T03:17:45.944Z
     * @returns date
     */
    static setDate(_date?: string): typeof DateKit;
    static getTimeStamp(): number;
    static getTotalDays(): number[];
    /**
     * Get what day of the week today is
     * @param _date pattern: YYYY-MM-DD HH:MM:SS
     * @returns week
     */
    static getWeekOfDate(_date?: string): "星期日" | "星期一" | "星期二" | "星期三" | "星期四" | "星期五" | "星期六";
    static format(format?: DateFormat): string;
    /**
     *  Calculate the difference between time intervals and return the difference in days, hours and minutes
     *
     * @param {*} startTime Starting time
     * @param {*} endTime End Time
     * @returns
     * {
     *    day: days,
     *    hours: hour,
     *    timeStr: time,
     *    minutes
     *  }
     */
    static calcDateDifference: (startTime: string, endTime: string) => {
        day: number;
        hours: number;
        timeStr: string;
        minutes: number;
    };
    /**
     * Get historical time
     * format: YYYY-MM-DD HH:MM:SS
     *         | YYYY
     *         | YYYY-MM
     *         | YYYY-MM-DD HH
     *         | YYYY-MM-DD HH:MM
     * @param { timestamps: string } date of value
     * @returns object{ time: xxx, unit: 'day', text: 'minutes ago'}
     */
    static getPastTime(timestamps: string): {
        time: number;
        unit: string;
        text: string;
    };
    static addZero(num: number | string): number | string;
    static zhCnTime(format?: DateFormat): string;
    static zhCnTime(format?: DateFormat, date?: Date | string): string;
}
declare const dateKit: typeof DateKit;
export { dateKit, DateKit };
