export declare const setZhCnTimeStr: (value_?: string, time_?: Date | string) => string;
export declare const addZero: (num: number | string) => string | number;
export declare const isTimeValid: (time_?: Date | string) => boolean;
