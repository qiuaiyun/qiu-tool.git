export declare const duplicationArr: <T, K extends keyof T>(arr: T[], key?: K | undefined) => T[];
