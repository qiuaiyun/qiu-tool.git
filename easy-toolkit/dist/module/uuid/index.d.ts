/**
 * Get unique id
 *
 * @author qiuny
 * @returns {string}
 */
type Char = '-' | '_' | '*' | '#' | '&';
declare const getUUID: (char?: Char) => string;
export { getUUID };
