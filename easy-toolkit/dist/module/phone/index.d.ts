declare const validatePhone: (telephone: string | number) => boolean;
export { validatePhone };
