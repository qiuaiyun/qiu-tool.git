export type Options = "weak" | "middle" | "powerful";
/**
 * Password verification
 *
 * @param { string } password value
 * @param { string } password level "weak"|"middle"|"powerful"
 * @returns { boolean } returns boolean value
 */
declare const validatePassword: (password: string, level?: Options) => boolean | string;
export { validatePassword, };
