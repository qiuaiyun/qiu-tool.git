/**
 * Implement array data grouping method based on type
 * Return the result as an object
 *
 * @param {*} array
 * @param {*} callBack
 * @returns
 */
declare const groupByKey: <T, K>(array?: T[] | undefined, fn?: callBack<T, K> | undefined) => Record<string, T[]>;
/**
 * 回调函数
 */
export type callBack<T, K> = (item: T) => K;
export { groupByKey };
export default groupByKey;
