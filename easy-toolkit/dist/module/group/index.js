"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.groupByKey = void 0;
/**
 * Implement array data grouping method based on type
 * Return the result as an object
 *
 * @param {*} array
 * @param {*} callBack
 * @returns
 */
const groupByKey = (array, fn) => {
    // jest will throw an exception about warn
    if (!Array.isArray(array) && array) {
        console.warn(`Make sure the parameter '${array}' is an array!`);
    }
    if (array && array.some(e => typeof e === 'string')) {
        console.warn(`The parameter ${array} is not an complete object array!`);
    }
    let newArray;
    if (!array)
        return {};
    else {
        newArray = array.reduce((pre, item) => {
            const key = JSON.stringify(fn && fn(item));
            if (!pre[key])
                pre[key] = [];
            pre[key].push(item);
            return pre;
        }, {});
    }
    return newArray;
};
exports.groupByKey = groupByKey;
exports.default = groupByKey;
//# sourceMappingURL=index.js.map