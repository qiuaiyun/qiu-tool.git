export declare const deepClone: <T extends string | object | null | undefined>(source: T) => T | T[];
