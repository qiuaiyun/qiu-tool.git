"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deepClone = void 0;
const deepClone = (source) => {
    if (source === null || source === undefined) {
        return source;
    }
    if (source instanceof Array) {
        return source.map(item => (0, exports.deepClone)(item));
    }
    if (typeof source === 'object') {
        const copy = Object.assign({}, source);
        const clone = Object.create(source);
        Object.keys(copy).forEach(key => clone[key] = (0, exports.deepClone)(copy[key]));
        return clone;
    }
    return source;
};
exports.deepClone = deepClone;
//# sourceMappingURL=index.js.map