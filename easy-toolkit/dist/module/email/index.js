"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateEmail = void 0;
/**
 * Verify whether the email format is legal
 *
 * @param {*} value email
 * @returns {} or Boolean
 */
const validateEmail = (email) => {
    if (!email) {
        return { error: 'email is not empty' };
    }
    const regX = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
    return regX.test(email);
};
exports.validateEmail = validateEmail;
//# sourceMappingURL=index.js.map