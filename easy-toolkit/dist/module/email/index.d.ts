/**
 * Verify whether the email format is legal
 *
 * @param {*} value email
 * @returns {} or Boolean
 */
export declare const validateEmail: (email?: string) => boolean | {
    error: string;
};
