/**
 * Get random names
 *
 * @author qiuny
 */
declare class Shuffle {
    /**
     * Get a random English name
     *
     * @param count Control quantity parameters
     */
    static englishNames(count?: number | string): Array<string>;
    static randomEnName(): string;
    /**
     * Get a random Chinese name
     *
     * @param count Control quantity parameters
     */
    static chineseNames(count?: number | string): Array<string>;
}
export { Shuffle };
