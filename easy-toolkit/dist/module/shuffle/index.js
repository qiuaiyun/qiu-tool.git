"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Shuffle = void 0;
const surnames_1 = require("./surnames");
/**
 * Get random names
 *
 * @author qiuny
 */
class Shuffle {
    static englishNames(count) {
        let tempArr = [];
        if (typeof Number(count) === "number" && Number(count) > 0) {
            for (let i = 0; i < Number(count); i++) {
                tempArr = tempArr.concat([Shuffle.randomEnName()]);
            }
            return tempArr.map(mp => mp.charAt(0).toUpperCase() + mp.slice(1));
        }
        const strName = Shuffle.randomEnName();
        return strName.charAt(0).toUpperCase() + strName.slice(1);
    }
    static randomEnName() {
        const vowels = "aeiou";
        const consonants = "bcdfghjklmnpqrstvwxyz";
        const nameLength = Math.floor(Math.random() * 4) + 5;
        let tempName = "";
        for (let i = 0; i < nameLength; i++) {
            if (i % 2 === 0) {
                tempName += vowels.charAt(Math.floor(Math.random() * vowels.length));
            }
            else {
                tempName += consonants.charAt(Math.floor(Math.random() * consonants.length));
            }
        }
        return tempName;
    }
    static chineseNames(count) {
        const randomSurname = Math.floor(Math.random() * surnames_1.surnamesData.length);
        const randomName = Math.floor(Math.random() * surnames_1.Names.length);
        if (typeof Number(count) === 'number' && Number(count) > 0) {
            let tmepArr = [];
            for (let i = 0; i < Number(count); i++) {
                const SurnameIndex = Math.floor(Math.random() * surnames_1.surnamesData.length);
                const NameIndex = Math.floor(Math.random() * surnames_1.Names.length);
                tmepArr = tmepArr.concat([`${surnames_1.surnamesData[SurnameIndex]}${surnames_1.Names[NameIndex]}`]);
            }
            return tmepArr;
        }
        return `${surnames_1.surnamesData[randomSurname]}${surnames_1.Names[randomName]}`;
    }
}
exports.Shuffle = Shuffle;
//# sourceMappingURL=index.js.map