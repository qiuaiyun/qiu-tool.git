/**
 * Group object type
 */
type GroupObjectType = {
    [key: string]: number | string | Array<string | number>;
};
type GroupObjectArray<T> = T[];
export { GroupObjectType, GroupObjectArray };
