import { DateKit as DateClass } from "./module/date";
import { DateFormat } from "./module/date/DateType";
import { callBack } from "./module/group";
import { Options } from "./module/password";
import { Shuffle as shuffle } from "./module/shuffle";
/**
 * class Shuffle
 *
 * @description Get a random English name or Chinese name
 */
export declare class Shuffle extends shuffle {
}
/**
 * Function
 *
 * @description Get unique uuid
 * @returns a 32-bit string
 */
export declare const getUUID: () => string;
/**
 * Function
 *
 * @description Password verification
 * @returns boolean|string
 */
export declare const validatePassword: (password: string, level?: Options) => boolean | string;
/**
 * Function
 *
 * @description Phone number verification.
 *
 * Check telephone format is right by this method.
 *
 * @returns boolean
 */
export declare const validatePhone: (telephone: string | number) => boolean;
/**
 * Function
 *
 * @description Verify whether the email format is legal.
 * @returns boolean
 */
export declare const validateEmail: (email?: string) => boolean | {
    error: string;
};
/**
 * Function
 *
 * @description Deep clone data, supporting objects, arrays, and strings.
 * @returns val
 */
export declare const deepClone: (val: any) => any;
/**
 * Function
 *
 * @description Array deduplication
 * @param arr a array value
 * @param key unique key of object
 * @returns Array<T>
 */
export declare const duplicationArr: <T, K extends keyof T>(arr: T[], key?: K | undefined) => T[];
/**
 * class DateKit
 * dateKit instance of DateKit
 *
 * @description Time operation class
 */
export declare const weekDays: readonly ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
export type { DateFormat };
export declare const DateKit: typeof DateClass;
export declare const dateKit: typeof DateClass;
/**
 * Function
 *
 * @description Implement array data grouping method based on type
 * @param val An object
 * @returns Record<string, unknown[]>
 */
export declare const groupByKey: <T, K>(array?: T[] | undefined, fn?: callBack<T, K> | undefined) => Record<string, T[]>;
declare const _default: {
    groupByKey: <T, K>(array?: T[] | undefined, fn?: callBack<T, K> | undefined) => Record<string, T[]>;
};
export default _default;
