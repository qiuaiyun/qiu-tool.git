import { getUUID } from "../src/module/uuid";

describe('deepClone', () => {
    test('测试不带分隔符uuid', () => {
        const res = getUUID()
        expect(res).toEqual(res)
    })
    test('测试带分隔符uuid', () => {
        const res = getUUID('-')
        expect(res).toEqual(res)
    })
})