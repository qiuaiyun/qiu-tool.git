import { Shuffle } from "../src/module/shuffle";

describe('Shuffle', () => {
    test('测试获取一个中文名字', () => {
        const res = Shuffle.chineseNames()
        console.log(res);
        expect(res).toEqual(res)
    })
    test('测试获取10个中文名字', () => {
        const res = Shuffle.chineseNames(10)
        console.log(res);
        expect(res).toEqual(res)
    })
    test('测试获取一个英文名字', () => {
        const res = Shuffle.englishNames()
        console.log(res);
        expect(res).toEqual(res)
    })
    test('测试获取10个英文名字', () => {
        const res = Shuffle.englishNames(10)
        console.log(res);
        expect(res).toEqual(res)
    })
})