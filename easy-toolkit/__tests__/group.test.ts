import { groupByKey } from "../src/module/group";
const list = [
    { name: "asparagus", type: "vegetables", quantity: 5 },
    { name: "bananas", type: "fruit", quantity: 0 },
    { name: "goat", type: "meat", quantity: 23 },
    { name: "cherries", type: "fruit", quantity: 5 },
    { name: "fish", type: "meat", quantity: 22 },
];
describe('groupByKey', () => {
    // 如果不穿参数
    test('没有参数执行结果', () => {
        const res = groupByKey()
        expect(res).toEqual({})
    })
    test('执行结果如下：', () => {
        const res = groupByKey(list,({ type }) => type)
        // jest会转义特殊字符，例如对象key的双引号 "vegetables"转化为'"vegetables"'，否则校验不通过
        expect(res).toEqual({
            '"vegetables"': [ { name: 'asparagus', type: 'vegetables', quantity: 5 } ],
            '"fruit"': [
              { name: 'bananas', type: 'fruit', quantity: 0 },
              { name: 'cherries', type: 'fruit', quantity: 5 }
            ],
            '"meat"': [
              { name: 'goat', type: 'meat', quantity: 23 },
              { name: 'fish', type: 'meat', quantity: 22 }
            ]
          })
    })
})