import { deepClone } from "../src/module/clone";

describe('deepClone', () => {
    test('测试空对象拷贝', () => {
        const res = deepClone({})
        expect(res).toEqual({})
    })

    test('接收到null应该返回null值', () => {
        const res = deepClone(null)
        expect(res).toEqual(null)
    })

    test('接到数组应该返回数组', () => {
        const arr = [{ name: '张三', age: 25 }, true, [1,2,3,4]]
        const res = deepClone(arr)
        expect(res).toEqual(arr)
    })
})