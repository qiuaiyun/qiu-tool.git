import { validateEmail as valiEmail } from "./module/email";
import { dateKit as instanceDate, DateKit as DateClass } from "./module/date";
import { weekDays as week } from "./module/date/weekDays";
import { DateFormat } from "./module/date/DateType";
import { groupByKey as group, callBack } from "./module/group";
import { deepClone as copyClone } from "./module/clone";
import { duplicationArr as deduplication } from "./module/deduplication";
import { validatePhone as validateTel } from "./module/phone";
import { validatePassword as validatePass, Options } from "./module/password";
import { getUUID as uuid } from "./module/uuid";
import { Shuffle as shuffle } from "./module/shuffle";
import sum from "./module/sum";

/**
 * class Shuffle
 * 
 * @description Get a random English name or Chinese name
 */
export class Shuffle extends shuffle {};

/**
 * Function
 * 
 * @description Get unique uuid
 * @returns a 32-bit string 
 */
export const getUUID = (): string => uuid();

/**
 * Function
 * 
 * @description Password verification
 * @returns boolean|string
 */
export const validatePassword = (password: string, level?: Options): boolean|string => validatePass(password, level);

/**
 * Function
 * 
 * @description Phone number verification.
 * 
 * Check telephone format is right by this method.
 * 
 * @returns boolean
 */
export const validatePhone = (telephone: string | number): boolean => validateTel(telephone);

/**
 * Function
 * 
 * @description Verify whether the email format is legal.
 * @returns boolean
 */
export const validateEmail = (email?: string): boolean | {
    error: string;
} => valiEmail(email);


/**
 * Function
 * 
 * @description Deep clone data, supporting objects, arrays, and strings.
 * @returns val
 */
export const deepClone = (val: any):any => copyClone(val);

/**
 * Function 
 * 
 * @description Array deduplication
 * @param arr a array value
 * @param key unique key of object
 * @returns Array<T>
 */
export const duplicationArr = <T, K extends keyof T>(arr: T[], key?: K):T[] => {
    return deduplication(arr, key)
}

/**
 * class DateKit
 * dateKit instance of DateKit
 * 
 * @description Time operation class
 */
export const weekDays = week;
export type { DateFormat };
export const DateKit = DateClass;
export const dateKit = instanceDate;


/**
 * Function
 * 
 * @description Implement array data grouping method based on type
 * @param val An object
 * @returns Record<string, unknown[]>
 */
export const groupByKey = <T, K>(array?: T[], fn?:callBack<T, K>):Record<string, T[]> => {
    return group(array)
}

export default {
    groupByKey
}