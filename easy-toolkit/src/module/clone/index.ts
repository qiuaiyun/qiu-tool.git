/**
 * Recursively deep copy data
 * 
 * @author qiuny
 */
interface TempObj {
    [key:string]: any
}

type ParamFunction = <T extends object | null | undefined | string> (source: T) => T | T[];

export const deepClone = <T extends object | null | undefined | string>(source: T): T | T[] => {
    if (source === null || source === undefined) {
        return source;
    }

    if (source instanceof Array) {
        return source.map(item => deepClone(item) as T)
    }

    if (typeof source === 'object') {
        const copy:TempObj = { ...source };        
        const clone = Object.create(source);
        Object.keys(copy).forEach(key => clone[key] = deepClone(copy[key]));
        return clone as T;
    }
    return source as T;
}