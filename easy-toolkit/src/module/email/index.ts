/**
 * Verify whether the email format is legal
 * 
 * @param {*} value email
 * @returns {} or Boolean
 */
export const validateEmail = (email?: string): boolean | {
    error: string;
} => {
    if (!email) {
        return { error: 'email is not empty' }
    }
    const regX = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
    return regX.test(email);
}