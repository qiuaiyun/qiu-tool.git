/**
 * Implement array data grouping method based on type
 * Return the result as an object
 * 
 * @param {*} array 
 * @param {*} callBack 
 * @returns 
 */
const groupByKey = <T, K>(array?: T[], fn?:callBack<T, K>):Record<string, T[]> => {
    // jest will throw an exception about warn
    if (!Array.isArray(array) && array) {
        console.warn(`Make sure the parameter '${array}' is an array!`)
    }
    if (array && array.some(e => typeof e === 'string')) {
        console.warn(`The parameter ${array} is not an complete object array!`)
    }
    let newArray:Record<string, T[]>;
    if (!array) return {}
    else {
        newArray = array.reduce((pre: Record<string, T[]>, item: T) => {
            const key = JSON.stringify(fn && fn(item));
            if (!pre[key]) pre[key] = []
            pre[key].push(item)
            return pre;
        }, {})
    }
    return newArray;
}

/**
 * 回调函数
 */
export type callBack<T, K> = (item: T) => K;


export {
    groupByKey
}

export default groupByKey;