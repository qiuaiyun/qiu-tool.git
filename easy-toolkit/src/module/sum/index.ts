function total (a:number = 0, b:number = 0):number {
    return a + b;
}
export {
    total
}
export default total;